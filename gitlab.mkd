Title: Gitlab
Description: Usage of the Gitlab instance for Exherbo.
CSS: /css/main.css

#{include head}

# Gitlab

* This will become a table of contents (this text will be scraped).
{:toc}

## Overview

The preferred method of contributing patches to Exherbo is via the new [Gitlab] instance.
It replaces the Gerrit instance we had used before.

## Initial setup

First of all you will have to create an account on our [Gitlab] instance. Do note that
other Gitlab accounts (e.g. ones created on Gitlab.com) won't work. Creating one on
[our instance of Gitlab](https://gitlab.exherbo.org) is pretty straight forward:

1. Go to our [Gitlab](https://gitlab.exherbo.org) instance and click on "Register".
2. Fill in your information.
3. Add your SSH key(s) for easier pushing via SSH instead of HTTPS.
   This guide assumes that you use SSH pushing/pulling.

## Cloning repositories

1. Go to [Gitlab's frontage](https://gitlab.exherbo.org/) after you've signed in and search for the repository you want to clone.
   Click on its name to open it.

2. Click on Fork.

3. Either:

* Clone your fork of it, e.g. `git clone git@gitlab.exherbo.org:<user name>/<repository>.git`.

* If you already cloned the repo you can add your fork as remote, e.g.
  `git remote add my-fork git@gitlab.exherbo.org:<user name>/<repository>.git`

## Submitting patches

1. Use a branch other than `master`. This is required because `master` is 
   a protected branch on Gitlab by default, which means one can't force push to it.
2. Work on your copy of the repository, commit.
3. Test your work locally, as described in [our workflow docs](/docs/workflow.html)
4. Push your changes: `git push <fork-remote> <branch>`. Do note that the remote will be
   `origin` if you cloned your fork directly and the name you supplied to `git remote add`
    if you added it as remote yourself.
5. Open a merge request (Gitlab will return a link to open one directly when you push to your fork).
6. Check the box `Allow edits from maintainers`. This allows core devs to rebase your merge request,
   which is required for fast-forward merges if HEAD has changed since you had submitted your merge request.

## Keeping forks updated

You should always ensure that you have the latest version of the repository you want
to work on before starting your work to avoid duplicating work and merge conflicts.
You can do so with:

    git pull <exherbo-remote> master --rebase

## Getting more information

In case you're still unsure on how to use Gitlab you can checkout Gitlab's superb
[documentation](https://docs.gitlab.com/ee/README.html).

[Gitlab]:               https://gitlab.exherbo.org/exherbo
--

Copyright 2018 Rasmus Thomsen

#{include CC_3.0_Attribution_ShareAlike}
#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->

